@if(count($items))
    @foreach($items as $key => $item)
        <div class="commit commit1 {{ ($item->getter_name) ? 'answer' : '' }}">
            <div class="user-avatar-block">
                <div class="avatar-circle">
                    @if($item->user->getImagePath('image', 'mini'))
                        <img src="{{ $item->user->getImagePath('image', 'mini') }}" alt="{{ $item->user()->fullname }}" class="user-avatar">
                    @else
                        <img src="{{ asset('/img/current-user.png') }}" alt="{{ $item->user()->fullname }}" class="user-avatar" style="background: #ccc;">
                    @endif
                </div>

                <a name="{{ route('video.show.replied', ['id' => $item->id]) }}"
                   class="show_replied {{ (count($item->children)) ? 'unload_replied_link' : 'hidden-element' }} show_replied_{{ $item->id }}">
                    жооптор
                </a>
                <span class="hidden-element commit_id">{{ $item->id }}</span>
            </div>
            <div class="user-commit-message">
                <p class="name">{{ $item->user->fullname }} <span class="text-muted">{{ $item->created_at }}</span></p>
                <p><span class="to-whom">{{ ($item->getter_name) ?: '' }}</span>{!!  nl2br($item->text) !!}</p>
                <div class="answer-link-block">
                    <span class="answer-link">жооп берүү</span>
                    <span class="hidden-element">{{ $item->user->fullname }}</span>
                    <span class="hidden-element">{{ $item->id }}</span>
                </div>
            </div>
        </div>

        <div class="replied_commits_{{ $item->id }}"></div>
    @endforeach
@endif