@extends('layouts.app')

@section('h1')
    @if($entity)
        {{ $entity->title }}
    @else
        Видео сабактар
    @endif
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/show-video.css') }}">
@endpush

@section('content')
    <div class="row content_blc">
        <div class="col-md-12">
            <div class="show-wrapper">
                @if($entity)
                    <div class="show-video-card">
                        <div class="video-block">
                            <video poster="{{ $entity->getImagePath('image', 'full') ?: asset('/img/video-default-poster.jpg') }}" controls class="show-video">
                                <source src="{{ $entity->getFilePath('file', 'video') }}" type="video/mp4">
                            </video>
                        </div>
                        <div class="video-card-body inline_blk">
                            <p class="video-title">{{ $entity->title }}</p>
                            <span class="video-author">{{ $entity->author }}</span>
                            <p class="video-date text-muted">{{ $entity->created_at }}</p>
                            <p class="preview">{!! $entity->description !!}</p>
                        </div>
                    </div>

                    <span class="hidden-element last_commit_id">{{ $minId }}</span>
                    <div class="commit-block">
                        @include('video.commits')

                        <div class="uploading_and_error"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            var send_ajax = false;
            var has_rest_commits = true;
            var parent_commits_count = "{{ $parentCommitsCount }}";
            var sending = false;

            $('.commits-block').on('mouseover', function() {
                if(parseInt(parent_commits_count, 10) > 6 && !sending) {
                    send_ajax = true;
                }
            });

            $('.commits-block').on('mouseout', function() {
                send_ajax = false;
            });

            function showErrorWindow() {
                var error = `
                <div class="uploading_rest_commits_error">
                    <p>Ой..!!!</p>
                    <p>Серверден катачылык кетти..</p>
                    <span class="close_warning_win">жабуу</span>
                </div>`;

                $('.uploading_and_error').html(error);

                $('.close_warning_win').on('click', function() {
                    $('.uploading_and_error').html(' ');
                    sending = false;
                });
            }

            $(window).scroll(function() {
                if(send_ajax && has_rest_commits && $(window).scrollTop() == $(document).height() - $(window).height()) {
                    var url = "{{ route('video.upload.commits', ['id' => $entity->id]) }}";
                    var last_commit_id = $('.last_commit_id').html();

                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: {'last_commit_id': last_commit_id},
                        beforeSend: function() {
                            sending = true;
                            $('.uploading_and_error').css('text-align', 'center');
                            var link = "{{ asset('/img/loading.gif') }}";
                            var loading = $(document.createElement('img')).attr('src', link).addClass('loading_img');
                            $('.uploading_and_error').append(loading);
                        },
                        success: function (data) {
                            if(data.success) {
                                $('.uploading_and_error').html(' ');
                                $('.commits-block').html($('.commits-block').html() + data.html);
                                $('.last_commit_id').html(data.minId);

                                setTimeout(function() {
                                    sending = false;
                                }, 1000);

                                if(!data.hasRestCommits) {
                                    has_rest_commits = false;
                                }

                                showReplied();
                                buildFormFields();
                            }
                        },
                        error: function (errors) {
                            console.log('Error:' + errors.status + ' ' + errors.statusText);
                            showErrorWindow();
                        },
                    });
                }
            });

            $('#commit-button').on('click', function(event) {
                event.preventDefault();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: $('#commit-form').attr('method'),
                    url: $('#commit-form').attr('action'),
                    data: $('#commit-form').serialize(),
                    beforeSend: function() {
                        $('#commit-button').val('Жөнөтүлүүдө...');
                        $('#commit-button').prop('disabled', true);
                    },
                    success: function (data) {
                        if(data.success) {
                            document.getElementById('commit-form').reset();
                            $('#parent_id').val('');
                            $('#getter_name').val('');

                            if(data.replied) {
                                var replied_block = '.replied_commits_' + data.parentId;
                                var show_replied_link = '.show_replied_' + data.parentId;
                                $(show_replied_link).html('жашыруу');
                                $(show_replied_link).css('display', 'block');

                                $(show_replied_link).unbind('click').click( function() {
                                    if($(this).html() === 'жооптор') {
                                        $(this).html('жашыруу');
                                    } else {
                                        $(this).html('жооптор');
                                    }

                                    $(replied_block).slideToggle();
                                });

                                $(replied_block).css('display', 'block');
                                $(replied_block).html(data.html);
                            } else {
                                $('.commits-block').html(data.html);
                                showReplied();
                            }

                            $('.commit-count').html(data.count + ' комментарий');
                            $('#commit-button').val('Жөнөтүү');
                            $('.last_commit_id').html(data.minId);
                            has_rest_commits = true;
                            buildFormFields();
                        }
                    },
                    error: function (errors) {
                        console.log('Error:' + errors.status + ' ' + errors.statusText);
                        document.getElementById('commit-form').reset();
                        $('#commit-button').val('Жөнөтүү');
                        showErrorWindow();
                    },
                });
            });

            function showReplied() {
                $('.unload_replied_link').on('click', function(event) {
                    event.preventDefault();
                    var unloadRepliedLink = $(this);
                    var replied_block = '.replied_commits_' + $(this).next().html();
                    var url = $(this).attr('name');

                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: {},
                        beforeSend: function() {
                            $(replied_block).css('text-align', 'center');
                            var link = "{{ asset('/img/loading.gif') }}";
                            var loading = $(document.createElement('img')).attr('src', link).addClass('loading_img');
                            $(replied_block).append(loading);
                        },
                        success: function (data) {
                            if(data.success) {
                                var replied_block = '.replied_commits_' + data.parentId;
                                $(replied_block).css('text-align', 'left');
                                $(replied_block).html(data.html);
                                unloadRepliedLink.html('жашыруу');
                                buildFormFields();
                                changeClassAndCallBack(unloadRepliedLink, replied_block);
                            }
                        },
                        error: function (errors) {
                            console.log('Error:' + errors.status + ' ' + errors.statusText);
                            $(replied_block).html(' ');
                            showErrorWindow();
                        },
                    });
                });

                function changeClassAndCallBack(elem, replied_block) {
                    elem.on('click', '');
                    elem.unbind('click').click( function() {
                        if($(this).html() === 'жооптор') {
                            $(this).html('жашыруу');
                        } else {
                            $(this).html('жооптор');
                        }

                        $(replied_block).slideToggle();
                    });
                }
            }

            function buildFormFields() {
                var answer_link = document.querySelectorAll('.answer-link');
                var commit_input = document.querySelector('.commit-input');

                for(var i = 0; i < answer_link.length; i++) {
                    answer_link[i].onclick = input_focus;
                }

                function input_focus() {
                    document.querySelector('#getter_name').value = this.nextElementSibling.innerHTML;

                    if($(this).hasClass('child')) {
                        document.querySelector('#parent_id').value = this.previousElementSibling.innerHTML;
                    } else {
                        document.querySelector('#parent_id').value = this.nextElementSibling.nextElementSibling.innerHTML;
                    }

                    commit_input.focus();
                }

                var commit_button = document.querySelector('#commit-button');
                commit_input.onkeyup = function () {
                    if (this.value.length > 0) {
                        commit_button.disabled = false;
                    } else {
                        commit_button.disabled = true;
                    }
                }

                commit_input.onfocus = function () {
                    document.querySelector('.textarea-block').style.borderBottomColor = '#0068B3';
                }

                commit_input.addEventListener('blur', function () {
                    document.querySelector('.textarea-block').style.borderBottomColor = '#C5E2DE';
                });
            }

            buildFormFields();
            showReplied();
        });
    </script>
@endpush