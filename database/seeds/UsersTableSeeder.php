<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert(
            [
                'id'             => 1,
                'fullname'       => 'Турарбек Боютов',
                'email'          => 'tboyutov@weltkind.com',
                'birthday'       => '1997-01-21',
                'password'       => '$2y$10$FMWoAfw.m6aOFo8PfhPq.en6SDYqjikcSRodu/YkJclms4olV8ktG', //321321321
                'type'           => 'admin',
                'super_admin'    => 1,
                'position'       => 'Мүдүр',
                'priority'       => 1,
                'remember_token' => Str::random(10),
                'created_at'     => now()
            ]
        );
    }
}
