<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCommits extends Model
{
    protected $table = 'video_commits';

    protected $fillable = ['parent_id', 'video_id', 'user_id', 'text', 'getter_name'];

    public function scopeOrder($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function getCommits($id)
    {
        return $this->where('video_id', $id)->where('parent_id', null)->with('user')->order()->take(6)->get();
    }

    public function getCommitsCount($id)
    {
        return $this->where('video_id', $id)->count();
    }

    public function getRepliedCommits($id)
    {
        return $this->where('parent_id', $id)->with('user')->orderBy('created_at', 'asc')->get();
    }

    public function getRestCommits($lastCommitId, $videoId)
    {
        return $this->where('id', '<', $lastCommitId)
            ->where('video_id', $videoId)->where('parent_id', null)->order()->take(6)->get();
    }

    public function getParentCommitsCount($id)
    {
        return $this->where('video_id', $id)->where('parent_id', null)->count();
    }

    public function video()
    {
        return $this->belongsTo(Video::class, 'video_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function children()
    {
        return $this->hasMany(VideoCommits::class, 'parent_id');
    }
}
