<?php

namespace App\Http\Controllers;

use App\VideoCommits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoCommitsController extends Controller
{
    public function getModel()
    {
        return new VideoCommits();
    }

    public function getRules()
    {
        return [
            'text' => 'required|max:65535'
        ];
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, $this->getRules(), [], []);

        $commit = [
            'video_id'    => $id,
            'user_id'     => Auth::user()->id,
            'parent_id'   => $request->input('parent_id'),
            'getter_name' => $request->input('getter_name'),
            'text'        => $request->input('text'),
        ];

        $this->getModel()->create($commit);

        $commitCount = $this->getModel()->getCommitsCount($id);

        if ($request->input('parent_id')) {
            return $this->showReplied($request->input('parent_id'), $commitCount);
        }

        $items = $this->getModel()->getCommits($id);
        $returnHTML = view('video._commits')->with('items', $items)->render();

        return response()->json([
            'success' => true,
            'replied' => false,
            'html'    => $returnHTML,
            'count'   => $commitCount,
            'minId'   => collect($items)->min('id')
        ]);
    }

    public function showReplied($id, $commitCount = null)
    {
        $items = $this->getModel()->getRepliedCommits($id);
        $returnHTML = view('video.replied')->with('items', $items)->render();

        return response()->json([
            'success'  => true,
            'replied'  => true,
            'parentId' => $id,
            'html'     => $returnHTML,
            'count'    => $commitCount
        ]);
    }

    public function uploadCommits(Request $request, $id)
    {
        $hasRestCommits = false;

        $items = $this->getModel()->getRestCommits($request->input('last_commit_id'), $id);
        $minId = collect($items)->min('id');
        $restCommits = $this->getModel()->where('id', '<', $minId)->get();

        if (count($restCommits)) {
            $hasRestCommits = true;
        }

        $returnHTML = view('video._commits')->with('items', $items)->render();

        return response()->json([
            'success'        => true,
            'html'           => $returnHTML,
            'minId'          => $minId,
            'hasRestCommits' => $hasRestCommits
        ]);
    }
}
